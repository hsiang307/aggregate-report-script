
import com.tibbo.aggregate.common.context.*;
import com.tibbo.aggregate.common.datatable.*;
import com.tibbo.aggregate.common.device.DisconnectionException;
import com.tibbo.aggregate.common.device.RemoteDeviceErrorException;
import com.tibbo.aggregate.common.event.EventUtils;
import com.tibbo.aggregate.common.protocol.RemoteServer;
import com.tibbo.aggregate.common.protocol.RemoteServerController;
import com.tibbo.aggregate.common.script.*;
import com.tibbo.aggregate.common.server.*;

import java.io.IOException;
import java.time.ZoneId;
import java.util.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static java.time.temporal.ChronoUnit.DAYS;

public class Main {

    public static void main(String[] args) {

        LocalDate startDate = LocalDate.of(2018, 11, 25);
        LocalDate endDate = LocalDate.of(2018, 11, 28);
        long daysBetween = DAYS.between(startDate, endDate);
        Date localStartDate = Date.from(startDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        Date localEndParsed = Date.from(endDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        System.out.println(daysBetween);
        try {


                DataTable parsedData = parseData(0, startDate, endDate);
                System.out.println(parsedData.toString());

        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    public static DataTable parseData(int environment, LocalDate startDate, LocalDate endDate) throws ScriptException {



        TableFormat tf = new TableFormat();
        tf.addField(FieldFormat.create("Date", FieldFormat.STRING_FIELD, "Date", true));
        tf.addField(FieldFormat.create("TimesUsed", FieldFormat.STRING_FIELD, "Times Used", true));
        tf.addField(FieldFormat.create("RunningTime", FieldFormat.STRING_FIELD, "Running Time", true));
        tf.addField(FieldFormat.create("StopButton", FieldFormat.STRING_FIELD, "Times Stop Button Pressed", true));
        tf.addField(FieldFormat.create("OutOfOrder", FieldFormat.STRING_FIELD, "Time Out of Order", true));
        tf.addField(FieldFormat.create("CashIntake", FieldFormat.STRING_FIELD, "Cash Intake", true));

        DataTable table = new DataTable(tf);


        for (long i=0; i<10; i++) {

            DataTable allData = getDataTable(0, startDate, endDate);
            //Date now = new Date();
            Date now = new Date();
            Date yesterday = new Date();
            now = (Date) allData.getRecords().get(0).getValue(2);

            yesterday.setTime(now.getTime() - 86400000);


            int machineused = 0;
            int stopbutton = 0;
            int outoforder = 0;
            int timesrefilled = 0;

            for (int j = 0; j < allData.getRecordCount(); j++) {
                if (allData.getRecords().get(j).getValue(10).equals(0)) {
                    machineused++;
                }
                if (allData.getRecords().get(j).getValue(10).equals(1)) {
                    stopbutton++;
                }
                if (allData.getRecords().get(j).getValue(10).equals(2)) {
                    outoforder++;
                }
                if (allData.getRecords().get(j).getValue(10).equals(3)) {
                    timesrefilled++;
                }


                double runningtime = machineused * 3.5;
                int cashintake = machineused * 50;

                DataRecord rec = table.addRecord();
                rec.setValue("Date", allData.getRecords().get(0).getValue(1));
                rec.setValue("TimesUsed", machineused);
                rec.setValue("RunningTime", runningtime);
                rec.setValue("StopButton", stopbutton);
                rec.setValue("OutOfOrder", outoforder);
                rec.setValue("CashIntake", cashintake);
            }
        }
        return table;
    }

    public static DataTable getDataTable(int environment, LocalDate startDate, LocalDate endDate) throws ScriptException {



        DataTable history = null;

        RemoteServer rls = new RemoteServer("localhost", RemoteServer.DEFAULT_PORT, "admin", "admin");

        // Creating server controller
        RemoteServerController rlc = new RemoteServerController(rls, true);

        // Connecting to the server
        try {
            rlc.connect();
        } catch (DisconnectionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (RemoteDeviceErrorException e) {
            e.printStackTrace();
        } catch (ContextException e) {
            e.printStackTrace();
        }

        // Authentication/authorization
        try {
            rlc.login();
        } catch (ContextException e) {
            e.printStackTrace();
        }

        // Getting context manager
        ContextManager cm = rlc.getContextManager();
        Context rootContext = cm.getRoot();
        Context c = rootContext.get("users.admin.devices.SaniHelmet");
        Context eventsContext = rootContext.get("events");

        try {
            history = eventsContext.callFunction(EventsContextConstants.F_GET, null, "users.admin.devices.SaniHelmet", "MEL", null, startDate, endDate); // Other parameters omitted
        } catch (ContextException e) {
            e.printStackTrace();
        }
        try {
            rlc.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (RemoteDeviceErrorException e) {
            e.printStackTrace();
        }
        return history;
    }
}