import com.tibbo.aggregate.common.context.*;
import com.tibbo.aggregate.common.datatable.*;
import com.tibbo.aggregate.common.script.*;
import com.tibbo.aggregate.common.server.*;
import com.tibbo.linkserver.*;
import com.tibbo.linkserver.context.*;
import com.tibbo.linkserver.script.*;       
import com.tibbo.aggregate.common.context.*;
import com.tibbo.aggregate.common.datatable.*;
import com.tibbo.aggregate.common.device.DisconnectionException;
import com.tibbo.aggregate.common.device.RemoteDeviceErrorException;
import com.tibbo.aggregate.common.event.EventUtils;
import com.tibbo.aggregate.common.protocol.RemoteServer;
import com.tibbo.aggregate.common.protocol.RemoteServerController;
import com.tibbo.aggregate.common.script.*;
import com.tibbo.aggregate.common.server.*;       
import java.io.IOException;
import java.util.List;      
import java.time.LocalDate;         
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.Calendar;
     
public class %ScriptClassNamePattern% implements Script
{
  public DataTable execute(ScriptExecutionEnvironment environment, DataTable parameters) throws ScriptException
  {        
        RemoteServerController rlc = login();
        List deviceList = getDevices(rlc);            
        DataTable dates = parameters;                              
        String StringStartDate = (String) parameters.getRecords().get(0).getValue(0);
        String StringEndDate = (String) parameters.getRecords().get(0).getValue(1);
                          
        try {      
              DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
              Date StartDate = dateFormat.parse(StringStartDate);
              Date EndDate = dateFormat.parse(StringEndDate);
              DataTable dailyReport = calculateDailyReport(StartDate, EndDate, rlc, deviceList);
  
              logout(rlc);
              return dailyReport;
              } catch (java.text.ParseException e) {
                   e.printStackTrace();
                   logout(rlc);
              }                             
       return null;
  }                      
                         
        public static Calendar setStartOfMonth(Calendar date){
        Calendar startOfMonth=date;
        startOfMonth.set(Calendar.DAY_OF_MONTH, 1);      
        startOfMonth.set(Calendar.HOUR_OF_DAY, 0);
        startOfMonth.set(Calendar.MINUTE, 0);
        startOfMonth.set(Calendar.SECOND, 0);
        startOfMonth.set(Calendar.MILLISECOND, 0);
        return startOfMonth;     
  }
              
  public static RemoteServerController login() throws ScriptException {
  
     RemoteServer rls = new RemoteServer("220.130.226.80", RemoteServer.DEFAULT_PORT, "admin", "be2010505");
  
    // Creating server controller
    RemoteServerController rlc = new RemoteServerController(rls, true);
    // Connecting to the server
    try {
        rlc.connect();
    } catch (DisconnectionException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    } catch (InterruptedException e) {
        e.printStackTrace();
    } catch (RemoteDeviceErrorException e) {
        e.printStackTrace();
    } catch (ContextException e) {
        e.printStackTrace();
    }
  
    // Authentication/authorization
    try {
        rlc.login();
    } catch (ContextException e) {
        e.printStackTrace();
    }  
    return rlc;
  }
  
 public static List getDevices(RemoteServerController rlc) throws ScriptException {
 ContextManager cm= rlc.getContextManager();
   Context rootContext = cm.getRoot();
      List deviceList = rootContext.get("users.admin.devices").getChildren();
 
  return deviceList;
}
  
  public static DataTable calculateDailyReport(Date StartDate, Date EndDate, RemoteServerController rlc, List deviceList) throws ScriptException {
           
         ContextManager cm= rlc.getContextManager();        
         
                              
        TableFormat tf = new TableFormat();
        tf.addField(FieldFormat.create("DeviceName", FieldFormat.STRING_FIELD, "Device Name 裝置名稱", ""));
        tf.addField(FieldFormat.create("Date", FieldFormat.STRING_FIELD, "Date 日期", true));
        tf.addField(FieldFormat.create("RunningTime", FieldFormat.STRING_FIELD, "Running Time 執行時間", true));
        tf.addField(FieldFormat.create("OutOfOrder", FieldFormat.STRING_FIELD, "Times Out of Order 執行異常次數", true));
        tf.addField(FieldFormat.create("StopButton", FieldFormat.STRING_FIELD, "Times Stop Button Pressed 緊急停止次數", true));
        tf.addField(FieldFormat.create("TimesUsed", FieldFormat.STRING_FIELD, "Times Used 總次數", true));
        tf.addField(FieldFormat.create("CashIntake", FieldFormat.STRING_FIELD, "Cash Intake 總金額", true));
              
         
              
     
            DataTable table = new DataTable(tf);             
            Calendar Start = Calendar.getInstance();
            Calendar End = Calendar.getInstance();
            Start.setTime(StartDate); 
            End.setTime(StartDate);
            Start = setStartOfMonth(Start); 
            End = setStartOfMonth(End); 
            Start.add(Calendar.MONTH, -1);
            String deviceName = "";
            String deviceNameShort = "";
            long  months = EndDate.getTime() - StartDate.getTime();
            months =  (months/2592000000L) + 2;          
            
            for (int k=0;k<deviceList.size();k++){      
            
                int totalmachineused = 0;
                int totalstopbutton = 0;
                int totaloutoforder = 0;
                int totaltimesrefilled = 0;
                double totalrunningtime = 0;
                int totalcashintake = 0;
                    
                for (long i=0; i<months; i++){
                          
                  End.add(Calendar.MONTH, 1); 
                  Start.add(Calendar.MONTH, 1); 
                  Date CurrentStartDate = Start.getTime();
                  Date CurrentEndDate = End.getTime();
                  
                 
                  deviceName = deviceList.get(k).toString();
                  String arr[] = deviceName.split(" ", 2);
                  
                  deviceName = "users.admin.devices." + arr[0];
                  deviceNameShort = arr[0];
                 
                  DataTable allData = getDataTable(0, null, CurrentStartDate, CurrentEndDate, cm, deviceName);
                  int machineused = 0;
                  int stopbutton = 0;
                  int outoforder = 0;
                  int timesrefilled = 0;
          
                  for (int j = 0; j < allData.getRecordCount(); j++) {
                      if (allData.getRecords().get(j).getValue(10).equals(0)) {
                          machineused++;
                          totalmachineused++;
                      }
                      if (allData.getRecords().get(j).getValue(10).equals(1)) {
                          stopbutton++;
                          totalstopbutton++;
                      }
                      if (allData.getRecords().get(j).getValue(10).equals(2)) {
                          outoforder++;
                          totaloutoforder++;
                      }
                      if (allData.getRecords().get(j).getValue(10).equals(3)) {
                          timesrefilled++;
                          totaltimesrefilled++;
                      }       
                  }
         
                                               
                        double runningtime = machineused * 3.5;
                        int cashintake = machineused * 50;                                     
                        totalrunningtime+=runningtime;
                        totalcashintake+=cashintake;                                         
                        DateFormat outputFormatter = new SimpleDateFormat("MMMM yyyy");
                        String output = outputFormatter.format(CurrentStartDate);    
                        DataRecord rec = table.addRecord();
                        rec.setValue("Date", output);
                        rec.setValue("DeviceName", deviceNameShort);
                       rec.setValue("TimesUsed", machineused);                     
                       rec.setValue("RunningTime", runningtime + " mins");
                       rec.setValue("StopButton", stopbutton);
                       rec.setValue("OutOfOrder", outoforder);
                       rec.setValue("CashIntake", cashintake);  
                                         
                 }                                                 
                        DataRecord rec = table.addRecord();
                        rec.setValue("Date", "Total");                    
                        rec.setValue("TimesUsed", totalmachineused);                     
                        rec.setValue("RunningTime", totalrunningtime + " mins");
                        rec.setValue("StopButton", totalstopbutton);
                        rec.setValue("OutOfOrder", totaloutoforder);
                        rec.setValue("CashIntake", totalcashintake); 
                       Start.setTime(StartDate); 
                         End.setTime(StartDate);
                         Start = setStartOfMonth(Start); 
                         End = setStartOfMonth(End); 
                         Start.add(Calendar.MONTH, -1);       
                    }
      
                 
              return table;
  
               
      }
      
      public static DataTable getDataTable(int environment, DataTable parameters, Date StartDate, Date EndDate, ContextManager cm, String deviceName) throws ScriptException {
  
          DataTable history = null;  
          Context rootContext = cm.getRoot();
          Context c = rootContext.get(deviceName);
          Context eventsContext = rootContext.get("events");
  
          try {
              history = eventsContext.callFunction(EventsContextConstants.F_GET, null, deviceName, "MEL", null, StartDate, EndDate); // Other parameters omitted
              //history = eventsContext.callFunction(EventsContextConstants.F_GET, null, device, "MEL", null, StartDate, EndDate); // Other parameters omitted
          } catch (ContextException e) {
              e.printStackTrace();
          }
          return history;
      }
      
             
      public static void logout(RemoteServerController rlc) {
        try {
            rlc.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (RemoteDeviceErrorException e) {
                e.printStackTrace();
            }
       return;
       }
}